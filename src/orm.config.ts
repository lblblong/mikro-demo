import { Options } from '@mikro-orm/core';
import { TsMorphMetadataProvider } from '@mikro-orm/reflection';
import { Logger } from '@nestjs/common';

const logger = new Logger('ORM');

const ormConfig = {
  dbName: ':memory:',
  type: 'sqlite',

  metadataProvider: TsMorphMetadataProvider,
  debug: true,
  logger: logger.log.bind(logger),
  entities: ['dist/entities/**/*.js'],
  entitiesTs: ['src/entities/**/*.ts'],
  migrations: {
    path: './migrations',
  },
} as Options;

export default ormConfig;
