import { EntityProperty, Platform, Type } from '@mikro-orm/core';

export class TsvectorType extends Type<string, string> {
  convertToDatabaseValue(value: string) {
    return value;
  }

  convertToJSValue(value: string) {
    return value;
  }

  getColumnType(prop: EntityProperty, platform: Platform) {
    return 'tsvector';
  }
}
