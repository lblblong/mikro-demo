const config = {
  db: {
    type: 'postgresql' as const,
    host: 'localhost',
    username: 'postgres',
    password: 'postgres',
    port: 5432,
    database: 'postgres',
  },
};

export default config;
