import { BaseEntity, PrimaryKey, Property, Entity } from '@mikro-orm/core';
import { TsvectorType } from 'src/orm/custom-type/tsvecotor';

@Entity()
export class Adopt extends BaseEntity<Adopt, 'id'> {
  @PrimaryKey()
  id: number;

  @Property()
  title: string;

  @Property()
  images: string[];

  @Property()
  address: string;

  @Property({ nullable: true })
  price?: number;

  @Property({ nullable: true })
  remark?: string;

  @Property({ type: TsvectorType, nullable: true })
  tsvector_result?: string;
}
